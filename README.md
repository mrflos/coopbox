# CoopBox

Transformer un Raspberry Pi 4 (appelé par la suite RP4 pour faire plus court) sous Debian Buster en serveur local pour coopérer.
Ferme à yeswiki, etherpad, scrumblr, nextcloud,.. plus si affinités !

## Pré-requis

### Matériel

- un RP4 allumé (accessible par le réseau si vous utilisez une autre machine)

### Logiciel

- Testé sur une [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) version desktop complète

## Installation

*Votre RP4 doit être connecté à l'Internet pour lancer l'installation*

- ouvrir le terminal (menu framboise > Accessoires > LXTerminal)
- copier / coller :

```bash
curl https://framagit.org/mrflos/coopbox/raw/master/install.sh | bash
```

## Mise à jour

*Votre RP4 doit être connecté à l'Internet pour lancer la mise à jour*

- ouvrir le terminal (menu framboise > Accessoires > LXTerminal)
- copier / coller :

```bash
curl https://framagit.org/mrflos/coopbox/raw/master/update.sh | bash
```
