#!/bin/sh

# Variables 
if [ -z ${HOSTNAME+x}          ]; then HOSTNAME="coopbox"; fi;
if [ -z ${COOPBOX_PATH+x}      ]; then COOPBOX_PATH="$HOME/CoopBox"; fi;
if [ -z ${COOPBOX_USER+x}      ]; then COOPBOX_USER="pi"; fi;
if [ -z ${COOPBOX_GROUP+x}     ]; then COOPBOX_GROUP="pi"; fi;
if [ -z ${DATABASE_PASS+x}     ]; then DATABASE_PASS="coopboxdbrootpassword"; fi;
if [ -z ${ETHERPAD_DATABASE+x} ]; then ETHERPAD_DATABASE="etherpad"; fi;
if [ -z ${ETHERPAD_DBUSER+x}   ]; then ETHERPAD_DBUSER="etherpad"; fi;
if [ -z ${ETHERPAD_DBPASS+x}   ]; then ETHERPAD_DBPASS="etherpadPassword"; fi;
if [ -z ${YESWIKI_DATABASE+x}  ]; then YESWIKI_DATABASE="yeswiki"; fi;
if [ -z ${YESWIKI_DBUSER+x}    ]; then YESWIKI_DBUSER="yeswiki"; fi;
if [ -z ${YESWIKI_DBPASS+x}    ]; then YESWIKI_DBPASS="yeswikiPassword"; fi;
if [ -z ${YESWIKI_BRANCH+x}    ]; then YESWIKI_BRANCH="cercopitheque"; fi;

echo "INSTALLATION DE LA COOPBOX"
echo "Mise à jour du système"
sudo apt update -y
sudo apt dist-upgrade -y
echo "Installation des paquets pour avoir un serveur opérationnel"
sudo apt install mariadb-server php-fpm php-mysql php-common php-cli php-gd php-curl php-intl php-mbstring php-bcmath php-imap php-xml php-zip nginx nodejs avahi-daemon redis-server git wget -y

echo "Sécurisation du serveur MYSQL"
#TODO automatiser : 
# sudo mysql_secure_installation
# sudo service mysql stop
# sudo mysqld --skip-grant-tables &
# sudo mysqladmin -u root flush-privileges password '*'
# sudo service mysql stop
# sudo service mysql start
# sudo mysqladmin -u root password "$DATABASE_PASS"
# mysql -u root -p"$DATABASE_PASS" -e "UPDATE mysql.user SET Password=PASSWORD('$DATABASE_PASS') WHERE User='root'"
# mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1')"
# mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.user WHERE User=''"
# mysql -u root -p"$DATABASE_PASS" -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\_%'"
# mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"

echo "Installation d'Etherpad"
sudo mkdir -p $COOPBOX_PATH
sudo chown -R $COOPBOX_USER:$COOPBOX_GROUP $COOPBOX_PATH
sudo -u $COOPBOX_USER git clone --branch master https://github.com/ether/etherpad-lite.git $COOPBOX_PATH/etherpad
sudo -u $COOPBOX_USER cp $COOPBOX_PATH/etherpad/settings.json.template $COOPBOX_PATH/etherpad/settings.json
#ligne 64 title = titre du pad
#ligne 83 "skinName": "colibris",
#mettre de la ligne 132 a 136  en commentaire avec /* .... puis */
#décommenter de la ligne 143 a 153
#  "dbType" : "mysql",
#  "dbSettings" : {
#    "user":     "etherpad",
#    "host":     "localhost",
#    "port":     3306,
#    "password": "etherpadpassword",
#    "database": "etherpad",
#    "charset":  "utf8mb4"
#  },
  
 # ligne 158 : changer le texte par défaut par "Écrire à plusieurs ici"
 # ligne 176 : mettre en francais "lang" : "fr"
sudo mysql -u root -p"$DATABASE_PASS" -e "CREATE DATABASE $ETHERPAD_DATABASE;"
sudo mysql -u root -p"$DATABASE_PASS" -e "GRANT CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE on $ETHERPAD_DATABASE.* to '$ETHERPAD_DBUSER'@'localhost' identified by '$ETHERPAD_DBPASS';"
sudo mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"

cat > etherpad.service <<EOF
[Unit]
Description=Etherpad (real-time collaborative document editing)
After=syslog.target network.target

[Service]
Type=simple
User=$COOPBOX_USER
Group=$COOPBOX_GROUP
Environment=NODE_ENV=production
Restart=always
ExecStart=$COOPBOX_PATH/etherpad/bin/run.sh

[Install]
WantedBy=multi-user.target
EOF

sudo mv etherpad.service /etc/systemd/system/etherpad.service
sudo systemctl enable etherpad
echo "il faut faire le fichier de conf $COOPBOX_PATH/etherpad/settings.json et sudo systemctl start etherpad "
#sudo systemctl start etherpad

# Installer le Wiki
echo "Installation de YesWiki"
sudo mysql -u root -p"$DATABASE_PASS" -e "CREATE DATABASE $YESWIKI_DATABASE;"
sudo mysql -u root -p"$DATABASE_PASS" -e "GRANT CREATE,ALTER,SELECT,INSERT,UPDATE,DELETE on $YESWIKI_DATABASE.* to '$YESWIKI_DBUSER'@'localhost' identified by '$YESWIKI_DBPASS';"
sudo mysql -u root -p"$DATABASE_PASS" -e "FLUSH PRIVILEGES"
sudo -u $COOPBOX_USER git clone --branch cercopitheque https://github.com/YesWiki/yeswiki.git $COOPBOX_PATH/yeswiki
sudo -u $COOPBOX_USER cd $COOPBOX_PATH/yeswiki
# TODO install composer, setup autoupdate, extension ferme

# NGINX
sudo rm /etc/nginx/sites-enabled/default

cat > yeswiki <<EOF
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root $COOPBOX_PATH/yeswiki;
    index index.php index.html;

    server_name $HOSTNAME.local;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.3-fpm.sock;
    }
}
EOF
sudo mv yeswiki /etc/nginx/sites-available/yeswiki
sudo ln -s /etc/nginx/sites-available/yeswiki /etc/nginx/sites-enabled/yeswiki
sudo systemctl restart nginx
# sudo nginx -t


# sudo  mousepad /etc/php/7.3/fpm/pool.d/www.conf 
# passer
# user = pi
# group = pi

# listen.owner = pi
# listen.group = pi


# sudo  mousepad /etc/php/7.3/fpm/php.ini
# max_execution_time = 120
# upload_max_filesize = 200M
# memory_limit = 512M
# max_input_time = 120

# sudo service php7.3-fpm restart
# sudo service php7.3-fpm status
# sudo service nginx restart

# sudo tail /var/log/php

# sudo tail /var/log/nginx/error.log
# , upstream: "fastcgi://unix:/var/run/php/php7.3-fpm.sock:", host: "localhost"
# 2019/11/08 12:16:24 [crit] 2284#2284: *1 connect() to unix:/var/run/php/php7.3-fpm.sock failed (13: Permission denied) while connecting to upstream, client: ::1, server: localhost, request: "GET / HTTP/1.1", upstream: "fastcgi://unix:/var/run/php/php7.3-fpm.sock:", host: "localhost"
# 2019/11/08 12:16:25 [crit] 2284#2284: *1 connect() to unix:/var/run/php/php7.3-fpm.sock failed (13: Permission denied) while connecting to upstream, client: ::1, server: localhost, request: "GET / HTTP/1.1", upstream: "fastcgi://unix:/var/run/php/php7.3-fpm.sock:", host: "localhost"
# 2019/11/08 12:16:38 [crit] 2284#2284: *5 connect() to unix:/var/run/php/php7.3-fpm.sock failed (13: Permission denied) while connecting to upstream, client: ::1, server: localhost, request: "GET / HTTP/1.1", upstream: "fastcgi://unix:/var/run/php/php7.3-fpm.sock:", host: "localhost"

# sudo ls -al /var/run/php/php7.3-fpm.sock

# sudo service php7.3-fpm stop
# sudo chmod 777 /var/run/php/php7.3-fpm.sock
# sudo service php7.3-fpm restart

# sudo apt install php-common php-cli php-gd php-mysql php-curl php-intl php-mbstring php-bcmath php-imap php-xml php-zip

# sudo service php7.3-fpm restart

# sudo mousepad /etc/nginx/nginx.conf
# client_max_body_size 200M;
# user pi pi;

# sudo service nginx restart


# https://github.com/YesWiki/yeswiki-extension-ferme

# @laurent les trucs à changer en cas de copie de carte
# Avoir des noms sympas sur le réseau genre http://coopbox1.local
# https://www.howtogeek.com/167190/how-and-why-to-assign-the-.local-domain-to-your-raspberry-pi/
# sudo apt install avahi-daemon

# sudo mousepad /etc/hosts
# => mettre des noms differents pour  raspberrypi : coopbox1, coopbox2, etc,..

# sudo mousepad /etc/hostname
# => mettre des noms differents pour  raspberrypi : coopbox1, coopbox2, etc,..

# et changer dans wakka.config.php l'url du wiki

# sudo reboot


# installer framemo (postit)

# sudo apt install redis-server
# sudo systemctl enable --now redis-server.service

# cd $HOME/CoopBox
# git clone https://framagit.org/colibris/framemo.git postit
# cd postit
# npm install
# node server.js --port 9002

# Lancer automatiquement framemo
# sudo mousepad /etc/systemd/system/postit.service
# ou sudo gedit /etc/systemd/system/postit.service
# copier coller :

# [Unit]
# Description=postit (real-time collaborative postit)
# After=syslog.target network.target

# [Service]
# Type=simple
# User=pi
# Group=pi
# Environment=NODE_ENV=production
# Restart=always
# WorkingDirectory=/home/pi/CoopBox/postit
# ExecStart=/usr/bin/nodejs /home/pi/CoopBox/postit/server.js --port 9002

# [Install]
# WantedBy=multi-user.target

# puis exec
# systemctl enable postit
# systemctl start postit

# service postit status